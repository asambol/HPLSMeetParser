package hr.stanga.meetparser

import java.io.File
import java.text.NumberFormat
import java.text.ParseException
import java.util.*
import kotlin.collections.HashMap

val guestPlace = 100;

class Parser(val name : String, var eventType: EventType = EventType.SBD) {

    val nameCorrections: HashMap<String, String> = hashMapOf(
            "Danijel Peršić" to "Daniel Peršič",
            "Danijel Peršič" to "Daniel Peršič",
            "Tomas Novak" to "Tomas Novak #2",
            "Ivan Schobloher" to "Ivan Schoblocher"
    )

    fun applyNameCorrection(name: String): String {
        if (nameCorrections.containsKey(name)) {
            return nameCorrections.get(name)!!
        } else {
            return name
        }
    }

    fun parse() {
        if (name.toLowerCase().contains("bench")) {
            eventType = EventType.B
        }

        val bufferedReader = File(name).bufferedReader()
        val lineList = mutableListOf<String>()

        println(Entry.getHeader())

        bufferedReader.useLines { lines -> lines.forEach { lineList.add(it) } }
        var currentSex: Sex? = null
        var currentAgeDivision: AgeDivision? = null
        var currentWeightDivision: WeightDivision? = null
        for (it in lineList) {
            if (!it.contains("Kategorija")) {
                val columns = it.split(';')

                if (columns[1] == "Žene") {
                    currentSex = Sex.FEMALE;
                } else if (columns[1] == "Muški") {
                    currentSex = Sex.MALE;
                } else {
                    // check for weight class
                    val divisionStr = columns[1].replace("-","")
                    val weightDivisionOpt = WeightDivision.forName(divisionStr)
                    if (weightDivisionOpt != null) currentWeightDivision = weightDivisionOpt
                }

                val ageDivisionOpt = AgeDivision.forCroName(columns[2])
                if (ageDivisionOpt != null) {
                    currentAgeDivision = ageDivisionOpt
                }

                if (currentAgeDivision != null && currentSex != null && currentWeightDivision != null) {
                    var idx = 2
                    val name = applyNameCorrection(columns[idx++].capitalizeEachWord().replace("\\s+".toRegex(), " "))
                    idx++
                    if (name.isNotEmpty()) {
                        val team = columns[idx++].capitalizeEachWord()
                        if (eventType == EventType.B) {
                            idx++
                        }
                        val weightKg = columns[idx++].toDoubleOrZero()
                        //idx++ // removed in 2019/03
                        if (eventType == EventType.B) {
                            idx += 2
                        }
                        var squat = DoubleArray(4)
                        var bench = DoubleArray(4)
                        var deadlift = DoubleArray(4)
                        if (eventType == EventType.SBD) {
                            squat[0] = columns[idx++].toDoubleOrZero()
                            squat[1] = columns[idx++].toDoubleOrZero()
                            squat[2] = columns[idx++].toDoubleOrZero()
                            idx++
                            squat[3] = columns[idx++].toDoubleOrZero()
                            idx++
                            // dirty fix - we don't have exact fail data, so we assume 2 same attempts are fail for first one
                            if (squat[3] < squat[0] || squat[0] == squat[1] || squat[3] == 0.0) {
                                squat[0] = -squat[0]
                            }
                            if (squat[3] < squat[1] || squat[1] == squat[2] || squat[3] == 0.0) {
                                squat[1] = -squat[1]
                            }
                            if (squat[3] < squat[2] || squat[3] == 0.0) {
                                squat[2] = -squat[2]
                            }
                        }
                        bench[0] = columns[idx++].toDoubleOrZero()
                        bench[1] = columns[idx++].toDoubleOrZero()
                        bench[2] = columns[idx++].toDoubleOrZero()
                        idx++
                        bench[3] = columns[idx++].toDoubleOrZero()
                        idx++
                        if (bench[3] < bench[0] || bench[0] == bench[1] || bench[3] == 0.0) {
                            bench[0] = -bench[0]
                        }
                        if (bench[3] < bench[1] || bench[1] == bench[2] || bench[3] == 0.0) {
                            bench[1] = -bench[1]
                        }
                        if (bench[3] < bench[2] || bench[3] == 0.0) {
                            bench[2] = -bench[2]
                        }

                        if (eventType == EventType.SBD) {
                            deadlift[0] = columns[idx++].toDoubleOrZero()
                            deadlift[1] = columns[idx++].toDoubleOrZero()
                            deadlift[2] = columns[idx++].toDoubleOrZero()
                            idx++
                            deadlift[3] = columns[idx++].toDoubleOrZero()
                            if (deadlift[3] < deadlift[0] || deadlift[0] == deadlift[1] || deadlift[3] == 0.0) {
                                deadlift[0] = -deadlift[0]
                            }
                            if (deadlift[3] < deadlift[1] || deadlift[1] == deadlift[2] || deadlift[3] == 0.0) {
                                deadlift[1] = -deadlift[1]
                            }
                            if (deadlift[3] < deadlift[2] || deadlift[3] == 0.0) {
                                deadlift[2] = -deadlift[2]
                            }
                        }
                        var total = columns[idx++].toDoubleOrNull()
                        if (squat[3] == 0.0 && eventType == EventType.SBD || bench[3] == 0.0 || deadlift[3] == 0.0 && eventType == EventType.SBD) {
                            total = null
                        }
                        var place = columns[idx++].replace(".", "").toIntOrNull() ?: -1
                        if (total == null) {
                            place = -1
                        }
                        var weightDivision = currentWeightDivision
                        if (!currentWeightDivision.kg.contains('+') && currentWeightDivision.kg.toDouble() < weightKg) {
                            place = guestPlace
                            weightDivision = weightDivision.next()
                        }
                        if (weightKg != 0.0) {
                            val entry = Entry(name = name, team = team, event = eventType, sex = currentSex, bodyWeightKg = weightKg,
                                    weightDivision = weightDivision, ageDivision = currentAgeDivision,
                                    squat1Kg = squat[0], squat2Kg = squat[1], squat3Kg = squat[2], best3SquatKg = squat[3],
                                    bench1Kg = bench[0], bench2Kg = bench[1], bench3Kg = bench[2], best3BenchKg = bench[3],
                                    deadlift1Kg = deadlift[0], deadlift2Kg = deadlift[1], deadlift3Kg = deadlift[2], best3DeadliftKg = deadlift[3],
                                    totalKg = total, place = place)
                            println(entry)
                        }
                    }
                }

            }
        }
    }
}


/**
 * place -1 = DQ
 * attempts - minus if unsuccessful
 */
class Entry(val name: String, val team: String, val event: EventType, val sex: Sex, val bodyWeightKg: Double,
            val weightDivision: WeightDivision, val ageDivision: AgeDivision,
            val squat1Kg: Double? = null, val squat2Kg: Double? = null, val squat3Kg: Double? = null, val best3SquatKg: Double? = null,
            val bench1Kg: Double? = null, val bench2Kg: Double? = null, val bench3Kg: Double? = null, val best3BenchKg: Double? = null,
            val deadlift1Kg: Double? = null, val deadlift2Kg: Double? = null, val deadlift3Kg: Double? = null, val best3DeadliftKg: Double? = null,
            val totalKg: Double?, val place: Int,
            val equipment: String = "Raw") {

    override fun toString(): String {
        return "$name,$team,$equipment,$event,${sex.shortName},$bodyWeightKg,${ageDivision.engNaziv},${weightDivision.kg}," +
                "${if (squat1Kg != 0.0) squat1Kg else ""},${if (squat2Kg != 0.0) squat2Kg else ""},${if (squat3Kg != 0.0) squat3Kg else ""},${if (best3SquatKg != 0.0) best3SquatKg else ""}," +
                "${if (bench1Kg != 0.0) bench1Kg else ""},${if (bench2Kg != 0.0) bench2Kg else ""},${if (bench3Kg != 0.0) bench3Kg else ""},${if (best3BenchKg != 0.0) best3BenchKg else ""}," +
                "${if (deadlift1Kg != 0.0) deadlift1Kg else ""},${if (deadlift2Kg != 0.0) deadlift2Kg else ""},${if (deadlift3Kg != 0.0) deadlift3Kg else ""},${if (best3DeadliftKg != 0.0) best3DeadliftKg else ""}," +
                "${if (totalKg != null && place > 0) totalKg else ""},${if (place == -1 || place == 0) "DQ" else if (place == guestPlace) "G" else place}"
    }

    companion object {
        fun getHeader() : String {
            return "Name,Team,Equipment,Event,Sex,BodyweightKg,Division,WeightClassKg," +
                    "Squat1Kg,Squat2Kg,Squat3Kg,Best3SquatKg," +
                    "Bench1Kg,Bench2Kg,Bench3Kg,Best3BenchKg," +
                    "Deadlift1Kg,Deadlift2Kg,Deadlift3Kg,Best3DeadliftKg," +
                    "TotalKg,Place"
        }
    }
}

enum class Sex(val shortName: String, val croName: String) {
    MALE("M", "Muški"), FEMALE("F", "Žene")
}

enum class WeightDivision(val kg: String) {
    M_53("53"),
    M_59("59"),
    M_66("66"),
    M_74("74"),
    M_83("83"),
    M_93("93"),
    M_105("105"),
    M_120("120"),
    M_120_PLUS("120+"),
    F_43("43"),
    F_47("47"),
    F_52("52"),
    F_57("57"),
    F_63("63"),
    F_72("72"),
    F_84("84"),
    F_84_PLUS("84+");

    companion object {
        fun forName(kg: String) : WeightDivision? {
            WeightDivision.values().forEach {
                if (it.kg == kg) return it
            }
            return null
        }
    }
}

enum class AgeDivision(val croNaziv: List<String>, val engNaziv: String) {
    SUBJUNIOR(listOf("Kadeti", "Kadet", "Sub Junior"), "Sub-Juniors"),
    JUNIOR(listOf("Junior", "Juniori"), "Juniors"),
    OPEN(listOf("Open", "Seniori"), "Open"),
    MASTER_1(listOf("Master I", "Master 1"), "Masters 1"),
    MASTER_2(listOf("Master II", "Master 2"), "Masters 2"),
    MASTER_3(listOf("Master III", "Master 3"), "Masters 3"),
    MASTER_4(listOf("Master IV", "Master 4"), "Masters 4");

    companion object {
        fun forCroName(croName: String) : AgeDivision? {
            AgeDivision.values().forEach {
                it.croNaziv.forEach() { croNazivIt ->
                    if (croNazivIt.toLowerCase() == croName.toLowerCase()) return it
                }
            }
            return null
        }
    }
}

enum class EventType {
    SBD, B
}

fun String.capitalizeEachWord() : String {
    val str = this.toLowerCase()
    val words = str.split(" ").toMutableList()
    var output = ""
    for(word in words){
        output += word.capitalize() +" "
    }
    output = output.trim()
    return output
}

fun String.toDoubleOrZero() : Double {
    val format = NumberFormat.getInstance(if (this.contains('.')) Locale.US else Locale.GERMAN)
    try {
        return format.parse(this).toDouble()
    } catch (e: ParseException) {
        return 0.0
    }
}

fun <T: Enum<T>> T.next(): T {
    val values = declaringClass.enumConstants
    val nextOrdinal = (ordinal + 1) % values.size
    return values[nextOrdinal]
}

fun main(args : Array<String>) {
    if (args.isNotEmpty()) {
        Parser(args[0]).parse()
    } else {
        Parser("pitomaca_bench.csv").parse()
    }
}